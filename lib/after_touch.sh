#!/bin/bash

after_touch()
{
    FILENAME=$1
    if [ -f $FILENAME ]
    then
	echo "File created."
    fi
}

export -f after_touch
