#!/bin/bash

after_mkdir()
{
    echo "Checking if the directory is created.."
    DIRECTORY=$1
    if [ -d $DIRECTORY ]
    then
	echo -e "\e[1mCreated. Happy H\e[31ma\e[32mc\e[33mk\e[34mi\e[35mn\e[36mg\e[0m!"
    fi
}

export -f after_mkdir
