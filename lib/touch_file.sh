#!/bin/bash
source "${HOME}/bin/lib/after_touch.sh"

touch_file()
{
    FILENAME=$1
    while [ -f $FILENAME ]
    do
	echo "File exists. Press 1 to overwrite, 2 to create another, other to exit."
	read ANSWER
	if [ $ANSWER == '1' ]
	then
	    echo "Sure? y/n"
	    read SURE
	    if [ $SURE == 'y' ]
	    then
	        rm $FILENAME
	    else
		exit 0
	    fi
	elif [ $ANSWER == '2' ] 
	then
	    echo "Enter new file name:"
	    read NEWFILENAME
	    FILENAME=$NEWFILENAME
	    continue
	else
	    exit 0
	fi
    done

    (touch $FILENAME && after_touch $FILENAME) || echo "File not created."
}
	
