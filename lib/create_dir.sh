#!/bin/bash
. "${HOME}/bin/lib/after_mkdir.sh"

create_dir()
{
    DIRNAME=$1
    FUNCTION=${2:-mkdir}

    while [ -d $DIRNAME ]
    do
	echo "Directory ${DIRNAME} exists. Create another? y/n"
	read ANSWER
	if [ $ANSWER == 'y' ]
	then
	    echo "Enter new name:"
	    read NEWDIRNAME
	    DIRNAME=$NEWDIRNAME
	    continue
	else
	    exit 0
	fi
    done
	  
    ($FUNCTION $DIRNAME && after_mkdir $DIRNAME) || echo "Failed to create directory. Check your method."
}

export -f create_dir
